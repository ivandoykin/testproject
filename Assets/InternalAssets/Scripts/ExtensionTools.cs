using System.Collections;
using UnityEngine;

public class ExtensionTools : MonoBehaviour
{
    public static ExtensionTools Instance { get; private set; }

    private void Start()
    {
        Instance = GetComponent<ExtensionTools>();    
    }

    public static void SmoothTranslate(Transform obj, Vector3 destinationPoint, float time = GameplayStater.MoveTime)
    {
        Instance.StartCoroutine(SmoothTransformTranslate(obj, destinationPoint, time));
    }

    private static IEnumerator SmoothTransformTranslate(Transform obj, Vector3 destinationPoint, float time)
    {
        Vector3 startingPos = obj.position;
        float elapsedTime = 0;

        while (elapsedTime < time)
        {
            obj.position = Vector3.Lerp(startingPos, destinationPoint, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }
}

