using System.Collections;
using UnityEngine;

public enum Position
{
    Right,
    Left,
    Forward,
    Center
}

public class CameraController : MonoBehaviour
{
    public void Move(Position position)
    {
        if (GameplayStater.State != GameState.Play)
            return;

        switch (position)
        {
            case Position.Forward:
                ExtensionTools.SmoothTranslate(transform, transform.position + new Vector3(1, 0.84f, 0));
                break;

            case Position.Left:
                ExtensionTools.SmoothTranslate(transform, transform.position + new Vector3(-2, 1, 0));
                break;

            case Position.Right:
                ExtensionTools.SmoothTranslate(transform, transform.position + new Vector3(2, -1, 0));
                break;
        }
    }
}
