using UnityEngine;

public enum GameState
{
    Play,
    Start,
    Lose
}

public class GameplayStater : MonoBehaviour
{
    public const float MoveTime = 0.7f;
    private static GameplayStater instance;

    [SerializeField] private GameObject loseMenu;
    [SerializeField] private GameObject playMenu;
    [SerializeField] private GameObject startMenu;

    public static GameState State { get; private set; }

    private void Start()
    {
        if (instance != null)
            Destroy(gameObject);

        instance = GetComponent<GameplayStater>();
        SetState(GameState.Start);
    }

    public static void SetState(GameState newState)
    {
        State = newState;

        instance.loseMenu.SetActive(State == GameState.Lose);
        instance.startMenu.SetActive(State == GameState.Start);
        instance.playMenu.SetActive(State == GameState.Play);
    }
}
