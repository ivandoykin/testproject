using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    public List<Transform> SpawnPoints;
    private Player player;

    private void Start()
    {
        player = FindObjectOfType<Player>();
    }

    private void Update()
    {
        if ((transform.position - player.transform.position).magnitude >= 22)
            Destroy(gameObject);
    }
}
