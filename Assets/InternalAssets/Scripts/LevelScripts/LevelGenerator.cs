using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class LevelGenerator : MonoBehaviour
{
    private const float generatingDistance = 18;
    [SerializeField] private List<ObstacleSO> obstacles;

    [SerializeField] private GameObject ladderPrefab;
    [SerializeField] private Transform ladderParent;
    [SerializeField] private Transform lastLadder;

    [SerializeField] private Vector3 spawnInterval;

    private Player player;

    private void Start()
    {
        player = FindObjectOfType<Player>();
    }

    private void Update()
    {
        if ((lastLadder.position - player.transform.position).magnitude < generatingDistance)
        {
            GameObject ladderObj = Instantiate(ladderPrefab, ladderParent);
            ladderObj.transform.position = lastLadder.position + spawnInterval;
            lastLadder = ladderObj.transform;

            int obstacleIndex = Random.Range(0, obstacles.Count);
            GameObject obstacle = Instantiate(obstacles[obstacleIndex].ObstaclePrefab, lastLadder);
            Ladder ladder = lastLadder.GetComponent<Ladder>();

            if (obstacles[obstacleIndex].IsCentric)
                obstacle.transform.position = ladder.SpawnPoints[0].position;

            else
                obstacle.transform.position = ladder.SpawnPoints[Random.Range(0, ladder.SpawnPoints.Count)].position;
        }
    }
}
