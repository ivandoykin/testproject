using UnityEngine;
using TMPro;

public class ScoreCounter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI totalScoreText;

    private int score = 0;

    private void Start()
    {
        Player.HadOneStep += AddPoint;
    }

    private void OnDestroy()
    {
        Player.HadOneStep -= AddPoint;
    }

    private void AddPoint()
    {
        score++;
        scoreText.text = score.ToString();
        totalScoreText.text = score.ToString();
    }
}
