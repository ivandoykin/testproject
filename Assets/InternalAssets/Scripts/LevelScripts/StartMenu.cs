using UnityEngine;
using Lean.Touch;
using System;

public class StartMenu : MonoBehaviour
{
    private const float blinkInterval = 0.75f;

    [SerializeField] private GameObject startDescriptionText;
    private float currentTime = 0f;
    private Action<LeanFinger> tap;

    private void Start()
    {
        tap = delegate(LeanFinger finger) 
        {
            if (GameplayStater.State != GameState.Start)
                return;

            StartGame();
        };

        LeanTouch.OnFingerTap += tap;
    }

    private void OnDestroy()
    {
        LeanTouch.OnFingerTap -= tap;
    }

    private void Update()
    {
        if (GameplayStater.State != GameState.Start)
            return;

        currentTime += Time.deltaTime;

        if (currentTime >= blinkInterval)
        {
            startDescriptionText.SetActive(!startDescriptionText.activeSelf);
            currentTime = 0;
        }
    }

    private void StartGame()
    {
        GameplayStater.SetState(GameState.Play);
    }
}
