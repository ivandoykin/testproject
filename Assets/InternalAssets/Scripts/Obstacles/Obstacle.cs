using UnityEngine;

public abstract class Obstacle : MonoBehaviour
{
    public bool IsCentric { get; protected set; } = false;
}
