using UnityEngine;

[CreateAssetMenu(fileName = "newObstacle", menuName = "CustomObjects/Obstacle", order = 1)]
public class ObstacleSO : ScriptableObject
{
    [SerializeField] private GameObject obstaclePrefab;
    public GameObject ObstaclePrefab
    {
        get
        {
            return obstaclePrefab;
        }
    }

    [SerializeField] private bool isCentric;
    public bool IsCentric
    {
        get
        {
            return isCentric;
        }
    }
}
