using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapSample : Trap
{
    private const float moveTime = 1.5f;
    private const float waitTime = 0.75f;

    [Range(200, 500)]
    [SerializeField] private float speed;
    [SerializeField] private float fundamentLength;

    private bool isMoving = false;
    private int directionKf = 1;

    private void Start()
    {
        IsCentric = true;
    }

    private IEnumerator WaitForNextMove(float time = moveTime + waitTime)
    {
        isMoving = true;
        yield return new WaitForSeconds(time);
        isMoving = false;
    } 

    private void Update()
    {
        transform.eulerAngles += new Vector3(0, 0, speed * Time.deltaTime);

        if (!isMoving)
        {
            ExtensionTools.SmoothTranslate(transform, transform.position - new Vector3(0, 0, fundamentLength) * directionKf, moveTime);
            directionKf *= -1;

            StartCoroutine(WaitForNextMove());
        }
    }
}
