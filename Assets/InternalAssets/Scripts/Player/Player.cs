using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Lean.Touch;
using System;

public class Player : MonoBehaviour
{
    public delegate void PlayerEvents();
    public static event PlayerEvents HadOneStep;

    private Position playerPosition = Position.Center;
    private PlayerGrounder playerGrounder;
    private CameraController cameraController;
    private Rigidbody body;
    private bool isMoving = false;

    private Action<LeanFinger> tap;
    private Action<LeanFinger> swipe;

    private void Start()
    {
        body = GetComponent<Rigidbody>();
        cameraController = FindObjectOfType<CameraController>();
        playerGrounder = GetComponentInChildren<PlayerGrounder>();

        tap = delegate (LeanFinger finger)
        {
            if (GameplayStater.State == GameState.Lose)
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

            if (GameplayStater.State != GameState.Play || isMoving)
                return;

            StartCoroutine(MoveCooldown());
            cameraController.Move(Position.Forward);
            StartCoroutine(ParabolicJump(1f, 2.5f));
        };
        LeanTouch.OnFingerTap += tap;

        swipe = delegate (LeanFinger finger)
        {
            if (GameplayStater.State != GameState.Play || isMoving)
                return;

            if (Mathf.Abs(finger.SwipeScreenDelta.normalized.y) > 0.45f)
                return;

            StartCoroutine(MoveCooldown());

            if (finger.SwipeScreenDelta.normalized.x < 0 && playerPosition != Position.Left)
            {
                cameraController.Move(Position.Left);
                ExtensionTools.SmoothTranslate(transform, transform.position + new Vector3(0, 0, 3.5f));
                ChangeDirection(Position.Left);
            }

            if (finger.SwipeScreenDelta.normalized.x > 0 && playerPosition != Position.Right)
            {
                cameraController.Move(Position.Right);
                ExtensionTools.SmoothTranslate(transform, transform.position + new Vector3(0, 0, -3.5f));
                ChangeDirection(Position.Right);
            }
        };
        LeanTouch.OnFingerSwipe += swipe;
    }

    private void OnDestroy()
    {
        LeanTouch.OnFingerTap -= tap;
        LeanTouch.OnFingerSwipe -= swipe;
    }

    private void ChangeDirection(Position position)
    {
        if (position == Position.Right)
        {
            if (playerPosition == Position.Left)
            {
                playerPosition = Position.Center;
                return;
            }
            if (playerPosition == Position.Center)
            {
                playerPosition = Position.Right;
                return;
            }
        }

        if (position == Position.Left)
        {
            if (playerPosition == Position.Right)
            {
                playerPosition = Position.Center;
                return;
            }
            if (playerPosition == Position.Center)
            {
                playerPosition = Position.Left;
                return;
            }
        }
    }

    private IEnumerator MoveCooldown(float time = GameplayStater.MoveTime)
    {
        isMoving = true;
        yield return new WaitForSeconds(time);
        isMoving = false;
    }

    private IEnumerator ParabolicJump(float distance, float height, float time = GameplayStater.MoveTime)
    {
        bool leaveFromGround = false;
        Vector3 basePos = transform.position;
        float x1 = 0;
        float x2 = distance;
        float x3 = (x1 + x2) / 2.0f;
        float a = height / ((x3 - x1) * (x3 - x2));

        for (float passed = 0.0f; passed < time;)
        {
            passed += Time.deltaTime;
            float f = passed / time;

            if (f > 1)
                f = 1;

            float x = distance * f;
            float y = a * (x - x1) * (x - x2);
            body.MovePosition(new Vector3(basePos.x + x + 0.15f * f, basePos.y + y, transform.position.z));

            if (!playerGrounder.IsGrounded)
                leaveFromGround = true;
            if (playerGrounder.IsGrounded && leaveFromGround)
            {
                body.MovePosition(new Vector3(basePos.x + distance, transform.position.y, transform.position.z));
                isMoving = false;
                HadOneStep();
                yield break;
            }

            yield return null;
        }
    }

    private void OnBecameInvisible()
    {
        Lose();
    }

    private void Lose()
    {
        GameplayStater.SetState(GameState.Lose);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Obstacle>() != null && GameplayStater.State == GameState.Play)
            Lose();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Obstacle>() != null && GameplayStater.State == GameState.Play)
            Lose();
    }
}
