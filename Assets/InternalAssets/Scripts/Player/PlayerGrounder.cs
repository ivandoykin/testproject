using UnityEngine;

public class PlayerGrounder : MonoBehaviour
{
    private bool isGrounded = false;
    public bool IsGrounded
    {
        get
        {
            return isGrounded;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponentInParent<Ladder>() != null)
            isGrounded = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponentInParent<Ladder>() != null)
            isGrounded = false;
    }
}
